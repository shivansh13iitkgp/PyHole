bool updateMetric( const realV X, metric* restrict g )
{
    real x = X.s1;
    real y = X.s2;
    real z = X.s3;
    real r = sqrt(x*x + y*y + z*z);
    real f = 1.0/r;
    real U = 1.0 + CONST_M*f;
    real mf3 = CONST_M*f*f*f;

    g->tt = -U*U;
    g->xx = 1.0/(U*U);
    g->yy = g->xx; g->zz = g->xx;
    g->tx = 0.0; g->ty = 0.0; g->xy = 0.0;
    g->xz = 0.0; g->yz = 0.0;

    g->dx_tt = 2*U*x*mf3;
    g->dx_xx = 2*x*mf3/(U*U*U);
    g->dx_yy = g->dx_xx; g->dx_zz = g->dx_xx;
    g->dx_tx = 0.0; g->dx_ty = 0.0; g->dx_xy = 0.0;
    g->dx_xz = 0.0; g->dx_yz = 0.0;

    g->dy_tt = 2*U*y*mf3;
    g->dy_xx = 2*y*mf3/(U*U*U);
    g->dy_yy = g->dy_xx; g->dy_zz = g->dy_xx;
    g->dy_tx = 0.0; g->dy_ty = 0.0; g->dy_xy = 0.0;
    g->dy_xz = 0.0; g->dy_yz = 0.0;

    g->dz_tt = 2*U*z*mf3;
    g->dz_xx = 2*z*mf3/(U*U*U);
    g->dz_yy = g->dz_xx; g->dz_zz = g->dz_xx;
    g->dz_tx = 0.0; g->dz_ty = 0.0; g->dz_xy = 0.0;
    g->dz_xz = 0.0; g->dz_yz = 0.0;

    return r<CONST_EH;
}
