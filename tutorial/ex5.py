import numpy as np
from gr_pyhole import metric, observer, propagator
from gr_pyhole.image import Image
from gr_pyhole.display import Display

class MyMetric(metric.CartesianMetric):
    ID = 'MY'   # short ID of this metric

    def __init__(self, m = 1.0):
        super(MyMetric, self).__init__()
        self.m = m
        self.EH = m*0.05

    def update(self):
        x, y, z = self.x[1:4]
        r = np.sqrt(x*x + y*y + z*z)
        f = 1.0/r
        U = 1.0 + self.m*f
        mf3 = self.m*f**3   # shortens later expressions

        self.tt = -U*U
        self.xx = self.yy = self.zz = 1.0/(U*U)

        self.dx_tt = 2*U*x*mf3
        self.dx_xx = self.dx_yy = self.dx_zz = 2*x*mf3/(U**3)

        self.dy_tt = 2*U*y*mf3
        self.dy_xx = self.dy_yy = self.dy_zz = 2*y*mf3/(U**3)

        self.dz_tt = 2*U*z*mf3
        self.dz_xx = self.dz_yy = self.dz_zz = 2*z*mf3/(U**3)

        return r<self.EH

g = MyMetric()
o = observer.Equirectangular(r=15.0, theta=np.pi/2)
p = propagator.CartesianCPU(o, g, Rsky=30.0)
p.TOLERANCE = 1e-6
i = Image(p, (128,128))
i.saveImage('ex5.png')
i.save('ex5.npz')
d = Display(i, p)
d.show()
