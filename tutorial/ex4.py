import numpy as np
from gr_pyhole.scene import Scene

scene = Scene()
scene.metric = 'configuration-2'
scene.theta = np.deg2rad(90.0)
scene.size = (256,256)
scene.tolerance = scene.LOW_ACCURACY

# chose one(!) of these
scene.raytrace()
scene.raytrace_parallel(__name__)
scene.raytrace_MPI()
scene.raytrace_GPU(device="CPU", platform_id=0, gpu_device_id=0, cpu_device_id=0, real=np.float64)

# hide the remainder from parallel processes
if __name__=='__main__':
	scene.load()
	scene.saveImage()
	scene.saveDescription()
